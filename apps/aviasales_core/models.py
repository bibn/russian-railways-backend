from datetime import datetime

from django.db import models


class Station(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=255)

    def to_dict(self):
        return {
            'code': self.id,
            'title': self.title,
        }

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Станция'
        verbose_name_plural = 'Станции'


class Train(models.Model):
    code = models.IntegerField(verbose_name='Номер поезда', primary_key=True)
    has_coupe = models.BooleanField(verbose_name='Купе')
    has_sv = models.BooleanField(verbose_name='СВ')
    has_platscart = models.BooleanField(verbose_name='Плацкарт')

    def to_dict(self):
        return {
            'code': self.code,
            'has_coupe': self.has_coupe,
            'has_sv': self.has_sv,
            'has_platscart': self.has_platscart,
        }

    def __str__(self):
        return str(self.code)

    class Meta:
        verbose_name = 'Поезд'
        verbose_name_plural = 'Поезда'


class Trip(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(verbose_name='Наименование маршрута', max_length=128)
    departure_time = models.DateTimeField(verbose_name='Дата и время отправление', default=datetime.now())
    train = models.ForeignKey(to=Train, verbose_name='Поезд', on_delete=models.CASCADE, default=None, null=True, blank=True)

    def to_dict(self):
        return {
            'id': self.id,
            'title': self.title,
            'departure_time': self.departure_time,
            'train': self.train.to_dict(),
        }

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Поездка'
        verbose_name_plural = 'Поездки'


class TripStation(models.Model):
    number = models.IntegerField(verbose_name='Порядковый номер остановки')
    station = models.ForeignKey(to=Station, on_delete=models.CASCADE)
    trip = models.ForeignKey(to=Trip, on_delete=models.CASCADE)
    to_arrive_time = models.DurationField()
    cost = models.IntegerField(verbose_name='Стоимость')

    def to_dict(self):
        return {
            'number': self.number,
            'station': self.station.to_dict(),
            'trip': self.trip.to_dict(),
            'to_arrive_time': self.to_arrive_time,
            'cost': self.cost,
        }

    def __str__(self):
        return f'{self.station} ~~~ {self.trip}'
