import json
from datetime import datetime, timedelta
from typing import List

from django.db.models import Q
from django.http import HttpRequest, JsonResponse
from django.views.decorators.csrf import csrf_exempt

from apps.aviasales_core.models import Station, Train, Trip, TripStation


@csrf_exempt
def filter_trains(request: HttpRequest):
    update_fields()

    data = json.loads(request.body)
    start_point = data.get('start_point')
    end_point = data.get('end_point')
    departure_time = datetime.fromisoformat(data['departure_time']) if data.get('departure_time') else datetime.now()

    if start_point == end_point:
        return JsonResponse({'result': []})

    available_trains: List[Trip] = Trip.objects.filter(
        Q(departure_time__date=departure_time)
        & Q(tripstation__station__title=start_point)
    ).all()

    result = []
    for a in available_trains:
        if a.tripstation_set.filter(station__title=end_point):
            stations = [s.to_dict() for s in a.tripstation_set.order_by('number').all()]

            first_index = 10000
            last_index = -1
            for s in stations:
                if s['station']['title'] == start_point:
                    first_index = s['number'] - 1

                if s['station']['title'] == end_point:
                    last_index = s['number']

            if first_index > last_index - 1:
                continue

            first_point_time = a.departure_time
            last_point_time = a.departure_time
            current_time = a.departure_time

            tt = [t.to_dict() for t in a.tripstation_set.all()]
            for i in tt[:last_index]:
                if i['number'] - 1 == first_index:
                    first_point_time = current_time
                    continue

                current_time += i['to_arrive_time']
                if i['number'] == last_index:
                    last_point_time = current_time
                    break

            sum_cost = sum([s['cost'] for s in stations[first_index + 1:last_index]])

            res_a = a.to_dict()
            res_a['sum_cost_platscart'] = sum_cost
            res_a['sum_cost_sv'] = sum_cost * 3
            res_a['sum_cost_coupe'] = sum_cost * 2
            res_a['trip_start_point'] = stations[0]['station']['title']
            res_a['trip_end_point'] = stations[-1]['station']['title']
            res_a['first_point_time'] = first_point_time
            res_a['last_point_time'] = last_point_time

            result.append(res_a)

    return JsonResponse({'result': result})


@csrf_exempt
def get_stations(request: HttpRequest):
    stations = Station.objects.all()
    return JsonResponse({'result': [s.to_dict() for s in stations]})


def update_fields():
    trips = Trip.objects.all()

    for t in trips:
        if datetime.now().date() <= t.departure_time.date():
            break

        c_time = t.departure_time + timedelta(days=1)
        t.departure_time = c_time
        t.save()
