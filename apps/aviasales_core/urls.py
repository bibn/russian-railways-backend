from django.urls import path
from . import views


urlpatterns = [
    path('filter_trains', views.filter_trains, name='filter_trains'),
    path('get_stations', views.get_stations, name='get_stations'),
]
