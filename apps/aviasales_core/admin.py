from django.contrib import admin
from .models import TripStation, Trip, Station, Train
from django import forms


@admin.register(Station)
class StationAdmin(admin.ModelAdmin):
    pass


@admin.register(Train)
class TrainAdmin(admin.ModelAdmin):
    pass


class TripStationInline(admin.TabularInline):
    model = TripStation


@admin.register(Trip)
class TripAdmin(admin.ModelAdmin):
    inlines = [TripStationInline, ]
