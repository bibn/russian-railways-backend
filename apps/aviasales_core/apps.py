from django.apps import AppConfig


class AviasalesCoreConfig(AppConfig):
    name = 'apps.aviasales_core'
